package com.axmedbek.issuemanagementtool.api;

import com.axmedbek.issuemanagementtool.dto.IssueDto;
import com.axmedbek.issuemanagementtool.service.impl.IssueServiceImpl;
import com.axmedbek.issuemanagementtool.util.ApiPaths;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(ApiPaths.IssueCtrl.CTRL)
public class IssueController {
    private final IssueServiceImpl issueService;

    public IssueController(IssueServiceImpl issueService){
        this.issueService = issueService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<IssueDto> getById(@PathVariable(value = "id",required = true) Long id){
        IssueDto issueDto = issueService.getById(id);
        return ResponseEntity.ok(issueDto);
    }

    @PostMapping("/")
    public ResponseEntity<IssueDto> createIssue(@Valid @RequestBody IssueDto issueDto){
        return ResponseEntity.ok(issueService.save(issueDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<IssueDto> updateIssue(@PathVariable(value = "id",required = true) Long id,
                                                    @Valid @RequestBody IssueDto issueDto){
        return ResponseEntity.ok(issueService.update(id,issueDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> delete(@PathVariable(value = "id",required = true) Long id){
        return ResponseEntity.ok(issueService.delete(id));
    }
}
