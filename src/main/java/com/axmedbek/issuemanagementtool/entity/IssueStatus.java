package com.axmedbek.issuemanagementtool.entity;

public enum IssueStatus {
    OPEN,
    CLOSED,
    IN_REVIEW,
    IN_PROGRESS,
    RESOLVED
}
