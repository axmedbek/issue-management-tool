package com.axmedbek.issuemanagementtool.repository;

import com.axmedbek.issuemanagementtool.entity.Issue;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IssueRepository extends JpaRepository<Issue,Long> {
}
