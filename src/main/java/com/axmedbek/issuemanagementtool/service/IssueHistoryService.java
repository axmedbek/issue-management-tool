package com.axmedbek.issuemanagementtool.service;

import com.axmedbek.issuemanagementtool.dto.IssueHistoryDto;
import com.axmedbek.issuemanagementtool.entity.Issue;
import com.axmedbek.issuemanagementtool.util.TPage;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IssueHistoryService {
    IssueHistoryDto save(IssueHistoryDto issueHistory);

    IssueHistoryDto getById(Long id);

    List<IssueHistoryDto> getByIssueId(Long id);

    TPage<IssueHistoryDto> getAllPageable(Pageable pageable);

    Boolean delete(IssueHistoryDto issueHistory);

    void addHistory(Long id, Issue issue);
}
