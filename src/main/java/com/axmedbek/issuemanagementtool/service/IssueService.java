package com.axmedbek.issuemanagementtool.service;

import com.axmedbek.issuemanagementtool.dto.IssueDto;
import com.axmedbek.issuemanagementtool.util.TPage;
import org.springframework.data.domain.Pageable;

public interface IssueService {
    IssueDto save(IssueDto issue);

    IssueDto getById(Long id);

    TPage<IssueDto> getAllPageable(Pageable pageable);

    Boolean delete(Long issue);

    IssueDto update(Long id, IssueDto project);
}
