package com.axmedbek.issuemanagementtool.service;

import com.axmedbek.issuemanagementtool.dto.UserDto;
import com.axmedbek.issuemanagementtool.util.TPage;
import org.springframework.data.domain.Pageable;

public interface UserService {
    UserDto save(UserDto user);

    UserDto getById(Long id);

    TPage<UserDto> getAllPageable(Pageable pageable);

    UserDto getByUsername(String username);
}
